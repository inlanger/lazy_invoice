import csv

from django.conf import settings
from django.http import HttpResponse
from django.utils import timezone
from django.db.models import Q
from django.shortcuts import get_object_or_404

from applications.invoices.models import Invoice
from applications.invoices.serializers import InvoiceSerializer
from applications.invoices.tasks import mail_sender


def download_invoice(unprocessed_invoices: str, email: str) -> HttpResponse:
    """
    Generates and downloads the invoices table
    """
    if not unprocessed_invoices:
        invoices = Invoice.objects.filter(
            Q(status='Rejected') &
            Q(date__range=[timezone.now().replace(day=1, hour=0, minute=0, second=0), timezone.now()])
        )
    else:
        invoices = Invoice.objects.filter(id__in=unprocessed_invoices)

    data = InvoiceSerializer(invoices, many=True).data
    response = HttpResponse(content_type='text/csv')
    response['invoices'] = f'attachment; filename="invoices.csv"'
    f = csv.writer(response)
    f.writerow(["id", "userprofile", "number", "amount", "notes", "status"])

    for unprocessed_invoices in data:
        f.writerow([
            unprocessed_invoices.get('id'),
            unprocessed_invoices.get('userprofile'),
            unprocessed_invoices.get('number'),
            unprocessed_invoices.get('amount'),
            unprocessed_invoices.get('notes'),
            unprocessed_invoices.get('status'),
        ])

        mail_sender('Payment info', f"Your invoice in processing", settings.EMAIL_HOST_USER, [f'{email}'])

    return response


def change_status_invoice(invoices: list, status: str, email: str) -> None:
    """
    Changes invoices status
    """
    for invoice_id in invoices:
        invoice = get_object_or_404(Invoice, id=invoice_id)
        invoice.status = status
        invoice.save()
        mail_sender(
            'Payment info', f"Your invoice is {status}",
            settings.EMAIL_HOST_USER, [f"{email}"]
        )
