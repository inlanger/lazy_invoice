from django.contrib import admin

from applications.invoices.models import Invoice

admin.site.register(Invoice)
