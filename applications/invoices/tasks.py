from celery import shared_task
from django.core.mail import send_mail


@shared_task
def mail_sender(subject, message, sender, receivers):
    return send_mail(subject, message, sender, receivers)
