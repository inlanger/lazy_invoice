from django.shortcuts import get_object_or_404
from drf_yasg import openapi
from rest_framework import serializers
from applications.invoices.models import Invoice
from applications.userprofiles.models import BankAccount
from applications.userprofiles.serializers import BankAccountSerializer


class InvoiceSerializer(serializers.ModelSerializer):
    bank = BankAccountSerializer(read_only=True)
    userprofile = serializers.CharField(source='bank.userprofile', read_only=True)
    notes = serializers.CharField(required=False)

    class Meta:
        model = Invoice
        fields = '__all__'

    def create(self, validated_data):
        bank = get_object_or_404(BankAccount, userprofile__user=self.context['request'].user)
        validated_data['bank'] = bank
        return Invoice.objects.create(**validated_data)


download_data_request_docs = [
    openapi.Parameter(
        'object_id',
        in_=openapi.IN_QUERY,
        type=openapi.TYPE_ARRAY,
        required=True,
        description='List of invoices ids',
        items=openapi.Items(type=openapi.TYPE_STRING),
    ),
]
