from django.http import HttpResponse
from django_filters.rest_framework import DjangoFilterBackend
from drf_yasg.utils import swagger_auto_schema
from rest_framework.decorators import action
from rest_framework.mixins import (
    ListModelMixin,
    CreateModelMixin,
    UpdateModelMixin
)
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from rest_framework.status import HTTP_200_OK
from rest_framework.views import APIView
from rest_framework.viewsets import GenericViewSet

from applications.invoices.models import Invoice
from applications.invoices.permissions import RoleAdmin
from applications.invoices.serializers import InvoiceSerializer, download_data_request_docs
from applications.invoices.service import download_invoice, change_status_invoice
from applications.userprofiles.models import UserProfile, BankAccount


class InvoiceViewSet(CreateModelMixin, ListModelMixin, UpdateModelMixin, GenericViewSet):
    """
    create:
    Updates status of invoice list

    list:
    Returns a list of all invoices

    list:
    Returns an information about current user

    update:
    Updates information about user

    create:
    Creates a new invoice
    """
    permission_classes = [IsAuthenticated]
    serializer_class = InvoiceSerializer
    queryset = Invoice.objects.all()
    filter_backends = [DjangoFilterBackend]
    filterset_fields = ['status']

    def get_queryset(self):
        if self.request.user.userprofile.role == UserProfile.RoleTypes.ADMIN:
            return Invoice.objects.all()
        else:
            bank = BankAccount.objects.filter(userprofile=self.request.user.userprofile)
            return Invoice.objects.filter(bank__in=bank)

    @swagger_auto_schema(
        methods=['patch'],
        responses={HTTP_200_OK: "Status of invoices were changed"},
        operation_description="Changing status of invoices",
        operation_id='invoice-status',
        request_body=InvoiceSerializer(),
    )
    @action(detail=False, methods=['patch'], url_path='change_status', permission_classes=[RoleAdmin])
    def change_status(self, *args, **kwargs):
        change_status_invoice(
            invoices=self.request.data.get('id'),
            status=self.request.data.get('status'),
            email=self.request.user.email
        )

        return Response("Status of invoices were changed", status=HTTP_200_OK)


class DownloadInvoiceSet(APIView):
    serializer_class = InvoiceSerializer
    queryset = Invoice.objects.all()
    permission_classes = [RoleAdmin]

    @swagger_auto_schema(
        operation_id='download-chosen-invoices',
        operation_description='Download a CSV file of chosen invoices',
        manual_parameters=download_data_request_docs,
        responses={HTTP_200_OK: 'File is ready to be downloaded'}
    )
    def get(self, request, *args, **kwargs):
        response = download_invoice(
            unprocessed_invoices=self.request.query_params.get('id'),
            email=self.request.user.email
        )

        return HttpResponse(response)
