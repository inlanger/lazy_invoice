from rest_framework.permissions import BasePermission

from applications.userprofiles.models import UserProfile


class RoleAdmin(BasePermission):
    def has_permission(self, request, view):
        return request.user.userprofile.role == UserProfile.RoleTypes.ADMIN
