from django.db import models

from applications.userprofiles.models import BankAccount
from applications.utils.models import UUIDModel


class Invoice(UUIDModel):
    class PaymentStatus(models.TextChoices):
        SENT = 'Sent'
        PAID = 'Paid'
        REJECTED = 'Rejected'

    file = models.FileField()
    bank = models.ForeignKey(BankAccount, on_delete=models.CASCADE)
    number = models.CharField(max_length=13, help_text='+380xxxxxxxxx', unique=True)
    amount = models.IntegerField()
    notes = models.TextField(null=True, blank=True)
    status = models.CharField(max_length=8, default=PaymentStatus.SENT, choices=PaymentStatus.choices)
    date = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return f'User - {self.bank.userprofile}; number - {self.number}'
