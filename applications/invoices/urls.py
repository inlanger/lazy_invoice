from django.urls import path
from rest_framework import routers

from applications.invoices.views import InvoiceViewSet, DownloadInvoiceSet

router = routers.SimpleRouter(trailing_slash=False)
router.register('', InvoiceViewSet, basename='invoices')

app_name = "invoices"

urlpatterns = [
    path('dowload', DownloadInvoiceSet.as_view(), name='download')
]

urlpatterns += router.urls
