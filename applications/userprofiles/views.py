from rest_framework.decorators import action
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated
from rest_framework.mixins import (
    ListModelMixin,
    CreateModelMixin,
    UpdateModelMixin,
    RetrieveModelMixin,
    DestroyModelMixin
)
from rest_framework.status import HTTP_404_NOT_FOUND, HTTP_200_OK
from rest_framework.viewsets import GenericViewSet
from drf_yasg.utils import swagger_auto_schema
from applications.userprofiles.serializers import (
    BankAccountSerializer,
    BankAccountSerializerDefault,
    UserProfileSerializer,
)
from applications.userprofiles.models import BankAccount, UserProfile
from applications.userprofiles.service import change_bank_account


class BankViewSet(
    ListModelMixin,
    CreateModelMixin,
    UpdateModelMixin,
    RetrieveModelMixin,
    DestroyModelMixin,
    GenericViewSet
):
    """
    list:
    Returns a list of bank accounts for a specific user

    create:
    Creates a new bank account for current user

    retrieve:
    Returns information about chosen bank account

    update:
    Updates information about bank account

    destroy:
    Deletes a chosen bank account
    """
    serializer_class = BankAccountSerializer
    permission_classes = [IsAuthenticated]
    lookup_field = 'id'

    def get_queryset(self):
        return BankAccount.objects.filter(userprofile=self.request.user.userprofile)

    @swagger_auto_schema(
        methods=['PUT'],
        operation_description="Setting up a main bank account",
        operation_id='default-bank',
        request_body=BankAccountSerializerDefault(),
        responses={
            HTTP_200_OK: BankAccountSerializer(),
            HTTP_404_NOT_FOUND: 'BankAccount was not found'
        }
    )
    @action(detail=True, methods=['put'])
    def change(self, request, *args, **kwargs):
        try:
            new_default = change_bank_account(
                userprofile=self.request.user.userprofile,
                bank_account_id=kwargs['id']
            )
        except BankAccount.DoesNotExist:
            return Response('BankAccount was not found', status=HTTP_404_NOT_FOUND)

        return Response(BankAccountSerializer(new_default).data, status=HTTP_200_OK)


class UserViewSet(ListModelMixin, UpdateModelMixin, CreateModelMixin, GenericViewSet):
    """
    list:
    Returns an information about current user

    update:
    Updates information about user
    """
    serializer_class = UserProfileSerializer
    permission_classes = [IsAuthenticated]

    def get_queryset(self):
        if self.request.user.userprofile.role == UserProfile.RoleTypes.ADMIN:
            return UserProfile.objects.all()
        else:
            return UserProfile.objects.filter(user=self.request.user)
