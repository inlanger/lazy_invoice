from applications.userprofiles.models import BankAccount, UserProfile


def change_bank_account(userprofile: UserProfile, bank_account_id: str) -> BankAccount:
    """
    Changes Bank account status
    """

    BankAccount.objects.filter(userprofile=userprofile).update(default=False)

    new_default = BankAccount.objects.get(id=bank_account_id)
    new_default.default = True
    new_default.save()

    return new_default
