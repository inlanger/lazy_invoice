from django.contrib import admin

from applications.userprofiles.models import UserProfile, BankAccount

admin.site.register(UserProfile)
admin.site.register(BankAccount)
