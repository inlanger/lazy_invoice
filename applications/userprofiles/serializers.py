from rest_framework import serializers

from applications.userprofiles.models import BankAccount, UserProfile


class UserProfileSerializer(serializers.ModelSerializer):
    username = serializers.CharField(source='user.username', required=False)
    last_name = serializers.CharField(source='user.last_name', required=False)
    first_name = serializers.CharField(source='user.first_name', required=False)
    user = serializers.CharField(read_only=True)

    class Meta:
        model = UserProfile
        fields = '__all__'


class BankAccountSerializer(serializers.ModelSerializer):
    userprofile = UserProfileSerializer(read_only=True)
    iban = serializers.CharField(max_length=29)

    class Meta:
        model = BankAccount
        fields = ('userprofile', 'currency', 'iban', 'id', 'default')

    def validate(self, attrs):
        if len(attrs.get('iban')) != 29:
            raise ValueError('Check the length of the iban')
        return attrs

    def create(self, validated_data):
        profile = self.context['request'].user.userprofile
        return BankAccount.objects.create(userprofile=profile, **validated_data)


class BankAccountSerializerDefault(serializers.ModelSerializer):
    class Meta:
        model = BankAccount
        fields = ('default',)
