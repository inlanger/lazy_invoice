from django.db import models
from django.contrib.auth.models import User
from applications.utils.models import UUIDModel


class UserProfile(UUIDModel):
    """
    Represents user profile model
    """

    class RoleTypes(models.TextChoices):
        ADMIN = 'Admin'
        USER = 'User'

    class PositionTypes(models.TextChoices):
        DEVELOPER = 'Developer'
        QA = 'QA'
        PROJECT_MANAGER = 'Project manager'
        CONTENT_MANAGER = 'Content manager'
        HAPPINESS_MANAGER = 'Happiness manager'
        SALES_MANAGER = 'Sales manager'
        LEAD_GENERATOR = 'Lead generator'

    user = models.OneToOneField(User, on_delete=models.CASCADE)
    address = models.TextField()
    phone_number = models.CharField(max_length=13, help_text='+380xxxxxxxxx', unique=True)
    position = models.CharField(max_length=20, null=True, blank=True, choices=PositionTypes.choices)
    role = models.CharField(max_length=5, choices=RoleTypes.choices, default=RoleTypes.USER)

    def __str__(self):
        return self.user.username


class BankAccount(UUIDModel):
    """
    Represents user's bank account
    """
    class CurrencyList(models.TextChoices):
        USD = 'USD'
        EUR = 'EUR'
        UAH = 'UAH'
        GBP = 'GBP'

    userprofile = models.ForeignKey(UserProfile, on_delete=models.CASCADE)
    currency = models.CharField(max_length=3, default=CurrencyList.EUR, choices=CurrencyList.choices)
    iban = models.CharField(max_length=29)
    default = models.BooleanField(default=False)

    def __str__(self):
        return f'Currency = {self.currency}; iban = {self.iban}'
