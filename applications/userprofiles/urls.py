from rest_framework import routers

from applications.userprofiles.views import BankViewSet, UserViewSet

router = routers.SimpleRouter(trailing_slash=False)
router.register('banks', BankViewSet, basename='banks')
router.register('', UserViewSet, basename='users')

app_name = "userprofile"

urlpatterns = []

urlpatterns += router.urls
