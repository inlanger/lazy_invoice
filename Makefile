APP_NAME=web
MPY=python3 manage.py


attach_web:
	docker-compose exec $(APP_NAME) /bin/bash

start_docker:
	docker-compose up -d

stop_docker:
	docker-compose stop

app_restart:
	docker-compose stop $(APP_NAME)
	docker-compose up -d $(APP_NAME)

app_logs:
	docker-compose logs -f $(APP_NAME)

all_logs:
	docker-compose logs -f --tail="100"

down_docker:
	docker-compose down

rebuild_docker:
	docker-compose up --force-recreate --build -d

force_rebuild_docker: down_docker rebuild_docker

shell:
	$(MPY) shell

web_migrate: clean
	docker-compose exec $(APP_NAME) /bin/bash -c "$(MPY) migrate"

web_admin:
	docker-compose exec $(APP_NAME) /bin/bash -c "$(MPY) createsuperuser"

web_shell:
	docker-compose exec $(APP_NAME) /bin/bash -c "$(MPY) shell"

docker_list:
	docker-compose ps

migrations:
	$(MPY) makemigrations

migrate:
	$(MPY) migrate

run:
	$(MPY) runserver

# lazy migrations running
lazy:
	$(MPY) makemigrations
	$(MPY) migrate

celery:
	celery -A lazy_invoice worker --loglevel=INFO

admin:
	$(MPY) createsuperuser