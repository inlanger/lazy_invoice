FROM python:3.9.6-buster

ENV PYTHONUNBUFFERED 1
ENV PYTHONDONTWRITEBYTECODE 1

USER root
RUN mkdir /app
WORKDIR /app
COPY ./ /app/

RUN apt update -y \
    && apt upgrade -y \
    && apt install python3-pip make build-essential libssl-dev zlib1g-dev libbz2-dev libreadline-dev libsqlite3-dev curl -y \
    && pip install -r /app/requirements.txt \
    && chmod 755 /app/entrypoint.sh

USER $USER

ENTRYPOINT ["/bin/bash", "entrypoint.sh"]
