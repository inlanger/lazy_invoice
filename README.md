# Lazy invoice
### Simple invoice managing system

## Built with

- [Python 3.9](https://docs.python.org/3.9/)
- [Django](https://www.djangoproject.com/)
- [Django Rest Framework](https://www.django-rest-framework.org/)
- [Django-Filter](https://django-filter.readthedocs.io/en/stable/)
- [Celery](https://github.com/celery/celery)
- [drf-yasg](https://github.com/axnsan12/drf-yasg)
- [PostgreSQL](https://www.postgresql.org/)


## Running locally
1. Create `local_settings.py` file in the `lazy_invoice/` directory and override settings (db, email settings, etc).

2. Run migrations with the next command:
```bash
  make migrate
```

3. Create a super user with the next command:
```bash
  make admin
```

4. Run the Django server with the next command:
```bash
    make run
```

5. The Django Admin panel will be available by the next address `http://127.0.0.1:8000/admin`

6. Make sure that you were logged in Django Admin Panel before the current step. API docs will be available by next urls:

`http://127.0.0.1:8000/swagger/` - API docs with the Swagger style


## Run project with the docker-compose
1. Check that 5432, 80, 8000 ports are free, otherwise docker will show you error message like `Port is bind`. 
   
2. Copy the `env.example` file and rename it to `env`. In this file, add missing values to variables. Variables for sending emails could be empty.
   
3. Run the next command. It will create everything what you need to work with project.
```bash
  make start_docker
```
   
4.Specify a superuser.
```bash
  make web_admin
```

5. The Django Admin panel will be available by the next address `http://127.0.0.1/admin`

6. Make sure that you were logged in Django Admin Panel before the current step. API docs will be available by next urls:

`http://127.0.0.1/api/swagger/` - API docs with the Swagger style
