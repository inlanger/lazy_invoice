#!/bin/bash

echo "Lazy Wombat application..."
cd /app

python manage.py collectstatic --noinput

python manage.py migrate

gunicorn lazy_invoice.wsgi:application --name lazy_invoice.conf --workers=2 --bind=0.0.0.0:8000 --reload --log-level=info --log-file=-
